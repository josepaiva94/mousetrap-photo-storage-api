/**
 * Photo storage API - photo model
 *
 * @author José C. Paiva <up201200272@fc.up.pt>
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PhotoSchema = new Schema({

    name: {
        type: String,
        index: true
    },
    user_id: {
        type: Schema.Types.ObjectId,
        index: true
    },
    device_id: { // ignored for now
        type: Schema.Types.ObjectId,
        index: true
    },
    mimetype: {
        type: String
    },
    created_at: {
        type: Date
    },
}, { timestamps: { createdAt: 'created_at' } });

module.exports = mongoose.model('Photo', PhotoSchema);
