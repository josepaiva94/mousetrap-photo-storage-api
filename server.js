/**
 * Photo storage API server
 *
 * @author José C. Paiva <up201200272@fc.up.pt>
 */

const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const cors = require('cors');
const mongoose = require('mongoose');
require('dotenv').config();

const privateRoutes = require('./routes/private');
const publicRoutes = require('./routes/public');

if (!process.env.AUTH0_DOMAIN || !process.env.AUTH0_CLIENT_ID) {
    throw 'Make sure you have AUTH0_DOMAIN and AUTH0_CLIENT_ID in your .env file';
}

// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open');
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
    throw 'Mongoose default connection error: ' + err;
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    throw 'Mongoose default connection disconnected';
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

// connect to Mongo
mongoose.connect((process.env.MONGO_URL || 'mongodb://localhost:27017') + '/mousetrap');

// instantiate the Express JS framework
const app = express();

app.set('port', process.env.PORT || 3001);

app.use(logger('dev')); // enable request logging
app.use(bodyParser.json()); // parse request
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cors()); // enable Cross-Origin Resource Sharing

// Authentication middleware. When used, the
// access token must exist and be verified against
// the Auth0 JSON Web Key Set
const authenticate = jwt({
    // Dynamically provide a signing key based on the kid in the header and the singing keys provided by the JWKS endpoint.
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `https://${process.env.AUTH0_DOMAIN}/.well-known/jwks.json`
    }),

    // Validate the audience and the issuer.
    audience: process.env.AUTH0_CLIENT_ID,
    issuer: `https://${process.env.AUTH0_DOMAIN}/`,
    algorithms: ['RS256']
});

// publicly available routes
app.use('/api/public', publicRoutes);

// private routes (require authentication)
app.use('/api', authenticate, privateRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler (prints stacktrace)
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

// production error handler (no stacktrace)
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: {}
    });
});

var server = app.listen(app.get('port'), function() {
    console.log('Server listening on port ' + server.address().port + '. Mode: ' + app.get('env'));
});
