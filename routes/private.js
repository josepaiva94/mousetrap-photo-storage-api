/**
 * Photo storage API private routes
 *
 * @author José C. Paiva <up201200272@fc.up.pt>
 */

const express = require('express');
const path = require('path')
const fs = require('fs');
const multer = require('multer');
const crypto = require('crypto');
const mime = require('mime-types');

// load model
const Photo = require('../models/photo');

// Setup upload settings

const UPLOAD_PATH = './public/photos';

const storage = multer.diskStorage({
    destination: function(req, file, cb) {

        var destFolder = path.join(UPLOAD_PATH, req.user.sub.split('|')[1]);

        if (!fs.existsSync(destFolder))
            fs.mkdirSync(destFolder);

        cb(null, destFolder);
    },
    filename: function(req, file, cb) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            if (err) return cb(err);

            cb(null, raw.toString('hex').substring(0, 11) + Date.now() + '.' + mime.extension(file.mimetype));
        });
    }
});

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1000000,
        files: 1,
        fields: 0
    },
    fileFilter: function(req, file, cb) {
        var filetypes = /jpg|jpeg|png|gif$/;
        var mimetype = filetypes.test(file.mimetype);
        var extname = filetypes.test(path.extname(file.originalname).toLowerCase());

        if (mimetype && extname) {
            return cb(null, true);
        }

        var err = new Error('Only images (jpg|jpeg|png|gif) allowed');
        err.status = 400;
        return cb(err);
    }
}).single('photo');


// Setup routes

const router = express.Router();

// Upload a photo
router.post('/photos', upload, function(req, res, next) {

    Photo.create({
        name: path.parse(req.file.filename).name,
        user_id: req.user.sub.split('|')[1],
        mimetype: req.file.mimetype
    }, function(err, photo) {
        if (err) return next(err);

        res.json({
            id: photo._id
        });
    });

});

// Retrieve a photo
router.get('/photos/:id', function(req, res, next) {

    if (!req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        var err = new Error('Your photo name is not valid');
        err.status = 400;
        return next(err);
    }

    var photo = Photo.findOne({
        _id: req.params.id
    }, function(err, photo) {
        if (err) return next(err);

        if (!photo) {
            var err = new Error('Photo not found');
            err.status = 404;
            return next(err);
        }

        var file = path.join(UPLOAD_PATH, String(photo.user_id),
            photo.name + '.' + mime.extension(photo.mimetype));

        fs.exists(file, function(exists) {
            if (!exists) {
                var err = new Error('Not found');
                err.status = 404;
                return next(err);
            }

            res.setHeader('Content-Type', photo.mimetype);
            fs.createReadStream(file).pipe(res);
        });
    });
});

// Retrieve photos from device
router.get('/devices/:id/photos', function(req, res, next) {

    if (!req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        var err = new Error('Your device id is not valid');
        err.status = 400;
        return next(err);
    }

    var limit = 10
    if (req.query.limit) {
        if (req.query.limit.match(/^[0-9]+$/)) {
            limit = parseInt(req.query.limit);
        }
    }

    Photo.
        find({
            user_id: req.params.id
        }).
        limit(limit).
        sort('-created_at').
        select('_id mimetype created_at').
        exec(function(err, photos) {
            if (err) return next(err);

            res.json(photos);
        });
});

// Delete photo
router.delete('/photos/:id', function(req, res, next) {

    if (!req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        var err = new Error('Your photo id is not valid');
        err.status = 400;
        return next(err);
    }

    var photo = Photo.findOneAndRemove({
        _id: req.params.id,
        user_id: req.user.sub.split('|')[1]
    }, function(err, photo) {
        if (err) return next(err);

        if (!photo) {
            var err = new Error('Photo not found');
            err.status = 404;
            return next(err);
        }

        var file = path.join(UPLOAD_PATH, String(photo.user_id),
            photo.name + '.' + mime.extension(photo.mimetype));

        fs.unlink(file, function(err) {
            if(err && err.code == 'ENOENT') {
                var err = new Error('Not found');
                err.status = 404;
                return next(err);
            } else if (err) {
                var err = new Error('Error removing file')
                return next(err);
            }

            res.status(200).json({ 'message': 'success' })
        });
    });
});

module.exports = router;
