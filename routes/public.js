/**
 * Photo storage API public routes
 *
 * @author José C. Paiva <up201200272@fc.up.pt>
 */

const express = require('express');

const router = express.Router();

router.get('/', function(req, res) {
    res.json({
        message: "Hello from a public endpoint! You don't need to be authenticated to see this."
    });
});

module.exports = router;
