# Photo Storage API for MouseTrap

This project contains a Node.js API that enables photo storage for MouseTrap. The
API is secured with Auth0. Its main functionality is to receive an image from an
authenticated user/device and return the id under which the photo is available. For
the sake of simplicity, the photos can be accessed by all authenticated users who
have the id of the photo.

Another important feature is the possibility to get ids of the last N photos taken
by a device. It is also possible for the owner to delete one of his photos.

## Getting Started

Clone the repo or download it.

## Requirements

Make sure that you have mongodb running at `mongodb://localhost:27017`

## Setup the `.env` File

You need to create an `.env` file in the project root with the following content.

```
AUTH0_CLIENT_ID=nxXq9GaUdUnaWEF0YhwBoaAmBy5ABRmj
AUTH0_DOMAIN=mousetrap.eu.auth0.com

```

## Install the Dependencies and Start the API

```bash
npm install
npm start
```

The API will be served at `http://localhost:3001`. You can set a different port by
adding `PORT` key to your .env file.

## Endpoints

The API includes these endpoints:

**POST** /api/photos

* A protected endpoint which allows the upload of a photo. Requires a valid JWT access token.

**GET** /api/photos/:id

* A protected endpoint which retrieves the photo identified by `id`. Requires a valid JWT access token.

**GET** /api/devices/:id/photos?limit=N

* A protected endpoint which allows to retrieve the last N photos of a device. Requires a valid JWT access token.

**DELETE** /api/photos/:id

* A protected endpoint which allows the owner to delete one of his photos. Requires a valid JWT access token.

**GET** /api/public

* An unprotected endpoint which returns a message on success. Does not require a valid JWT access token.
* This is just a test route.

## Issue Reporting

If you have found a bug or if you have a feature request, please report them at this repository issues section. Please do not report security vulnerabilities on the public issues section, instead send an email
to up201200272 [at] fc.up.pt

## Author

[José C. Paiva](#)

## License

This project is licensed under the MIT license. See the [LICENSE](LICENSE.txt) file for more info.
